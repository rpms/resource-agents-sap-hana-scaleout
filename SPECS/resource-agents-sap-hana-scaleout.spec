#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the SAPHanaSR-ScaleOut upstream git repo.
#
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="SAPHanaSR-ScaleOut-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global upstream_prefix ClusterLabs-resource-agents
%global upstream_version e711383f

%global saphana_scaleout_prefix SAPHanaSR-ScaleOut
%global saphana_scaleout_hash c2af06c

Name:		resource-agents-sap-hana-scaleout
Summary:	SAP HANA Scale-Out cluster resource agents
Epoch:		1
Version:	0.180.0
Release:	4%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}
License:	GPLv2+
URL:		https://github.com/SUSE/SAPHanaSR-ScaleOut
%if 0%{?fedora} || 0%{?centos_version} || 0%{?rhel}
Group:		System Environment/Base
%else
Group:		Productivity/Clustering/HA
%endif
Source0:	%{upstream_prefix}-%{upstream_version}.tar.gz
Source1:	%{saphana_scaleout_prefix}-%{saphana_scaleout_hash}.tar.gz
Patch0:		bz2026278-1-SAPHanaController-SAPHanaTopology-add-systemd-support.patch
Patch1:		bz2026278-2-SAPHanaController-SAPHanaTopology-suppress-systemctl-output.patch
Patch2:		bz2026278-3-SAPHanaController-SAPHanaTopology-fix-list-unit-files-issue.patch
Patch3:		bz2050196-SAPHanaController-SAPHanaTopology-fix-metadata-version.patch

BuildArch:	noarch

BuildRequires:	automake autoconf gcc
BuildRequires:	perl-interpreter python3-devel
BuildRequires:	libxslt glib2-devel

%if 0%{?fedora} || 0%{?centos_version} || 0%{?rhel}
BuildRequires: docbook-style-xsl docbook-dtds
%endif

Requires:	resource-agents >= 4.1.1
Conflicts:	resource-agents-sap-hana

Requires:	/bin/bash /usr/bin/grep /bin/sed /bin/gawk
Requires:	perl

%description
The SAP HANA Scale-Out resource agents interface with Pacemaker
to allow SAP HANA Scale-Out instances to be managed in a cluster
environment.

%prep
%setup -q -n %{upstream_prefix}-%{upstream_version}
%setup -q -T -D -a 1 -n %{upstream_prefix}-%{upstream_version}

# add SAPHana agents to Makefile.am
mv %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/ra/SAPHana* heartbeat
sed -i -e '/			SAPInstance		\\/a\			SAPHanaController			\\\n			SAPHanaTopology		\\' heartbeat/Makefile.am
sed -i -e '/                          ocf_heartbeat_SAPInstance.7 \\/a\                          ocf_heartbeat_SAPHanaController.7 \\\n                          ocf_heartbeat_SAPHanaTopology.7 \\' doc/man/Makefile.am

cp %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/doc/LICENSE .

%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1

%build
if [ ! -f configure ]; then
	./autogen.sh
fi

%global rasset linux-ha

%configure BASH_SHELL="/bin/bash" \
	PYTHON="%{__python3}" \
	%{conf_opt_fatal} \
%if %{defined _unitdir}
    --with-systemdsystemunitdir=%{_unitdir} \
%endif
%if %{defined _tmpfilesdir}
    --with-systemdtmpfilesdir=%{_tmpfilesdir} \
    --with-rsctmpdir=/run/resource-agents \
%endif
	--with-pkg-name=%{name} \
	--with-ras-set=%{rasset}

%if %{defined jobs}
JFLAGS="$(echo '-j%{jobs}')"
%else
JFLAGS="$(echo '%{_smp_mflags}')"
%endif

make $JFLAGS

%install
make install DESTDIR=%{buildroot}

# remove other agents
find %{buildroot}/usr/lib/ocf ! -type d ! -iname "SAPHana*" -exec rm {} \;
find %{buildroot}/%{_mandir} -type f ! -iname "*SAPHana*" -exec rm {} \;

install -m 0755 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/bin/{SAPHanaSR-monitor,SAPHanaSR-showAttr} %{buildroot}/%{_sbindir}
mkdir %{buildroot}/%{_usr}/lib/SAPHanaSR-ScaleOut
install -m 0444 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/test/SAPHanaSRTools.pm %{buildroot}/%{_usr}/lib/SAPHanaSR-ScaleOut/SAPHanaSRTools.pm
mkdir -p %{buildroot}/%{_datadir}/SAPHanaSR-ScaleOut/samples
install -m 0644 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/srHook/SAPHanaSR.py %{buildroot}/%{_datadir}/SAPHanaSR-ScaleOut
install -m 0444 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/crmconfig/* %{buildroot}/%{_datadir}/SAPHanaSR-ScaleOut/samples
install -m 0444 %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/srHook/global.ini %{buildroot}/%{_datadir}/SAPHanaSR-ScaleOut/samples
gzip %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/man/SAPHanaSR*.?
cp %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/man/SAPHanaSR*.7.gz %{buildroot}/%{_mandir}/man7
cp %{saphana_scaleout_prefix}-%{saphana_scaleout_hash}/SAPHana/man/SAPHanaSR*.8.gz %{buildroot}/%{_mandir}/man8

## tree fixup
# remove docs (there is only one and they should come from doc sections in files)
rm -rf %{buildroot}/usr/share/doc/resource-agents

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%license LICENSE
%{_usr}/lib/ocf/resource.d/heartbeat/SAPHana*
%{_mandir}/man7/*SAPHana*
%{_mandir}/man8/*SAPHana*
%{_sbindir}/SAPHanaSR*
%{_usr}/lib/SAPHanaSR-ScaleOut
%{_datadir}/SAPHanaSR-ScaleOut

%exclude /etc
%exclude /usr/include
%exclude /usr/lib/debug
%exclude /usr/lib/systemd
%exclude /usr/lib/tmpfiles.d
%exclude /usr/libexec
%exclude /usr/sbin/ldirectord
%exclude /usr/sbin/ocf*
%exclude /usr/share/%{name}
%exclude /usr/src
%exclude %{_mandir}/man8/SAPHanaSR-manageAttr.8.gz

%changelog
* Thu Feb  3 2022 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.180.0-4
- SAPHanaController/SAPHanaTopology: follow OCF standard for version
  and OCF version in metadata

  Resolves: rhbz#2050196

* Tue Feb  1 2022 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.180.0-3
- SAPHanaController/SAPHanaTopology: add systemd support

  Resolves: rhbz#2026278

* Mon Aug  2 2021 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.180.0-1
- Add HANA MTR multi-site cluster support

  Resolves: rhbz#1987634

* Thu Apr 30 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.164.0-1
- Set default timeouts based on recommendations and a couple of bugfixes

  Resolves: rhbz#1827107

* Tue Feb 18 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.163.2-6
- Add Conflicts: to avoid future CI gating errors

  Resolves: rhbz#1802995

* Thu Jun 27 2019 Oyvind Albrigtsen <oalbrigt@redhat.com> - 0.163.2-5
- Initial build as separate package

  Resolves: rhbz#1705765
